# n �simo n�mero sequ�ncia Saw #

## Narrativa ##

Voc� acabou de entrar no universo do serial killer Jigsaw. Muito cuidado o portal est� repleto de armadilhas e voc� precisa tomar muito cuidado. Pense bem, sempre existe alguma forma de escapar. Nesse portal voc� precisar� resgatar a Joia da Alma que d� a seu portador o poder de roubar, manipular e alterar almas, seja dos vivos ou mesmo dos mortos.

Ao entrar no portal voc� ficou desacordado por algumas horas, Jigsaw o capturou e o colocou em um de seus jogos onde voc� ter� que escolher entre a vida e a morte. Concentre-se para escapar e recuperar a Joia da Alma. N�o se esque�a, a sobreviv�ncia da terra depende de voc�.

Voc� est� acorrentado a uma bomba, o contador iniciou, voc� tem exatamente 90 minutos para remover a bomba, assim que removida a mesma ser� automaticamente desativada.

Como parte do jogo mortal voc� ter� que resolver uma complexa tarefa para descobrir a senha do cadeado que ir� desarmar a bomba. As dicas para resolu��o do problema se encontram em um papel abaixo da cadeira. Que comecem os jogos!

**__________________**

## Desafio / Algoritmo: ##

A senha para desativar a bomba � o **68�** n�mero da sequ�ncia Saw. Essa sequ�ncia inicia pela soma do n�mero inteiro 0 ao primeiro n�mero da sequ�ncia de n�meros primos, o resultado obtido � novamente somado ao pr�ximo n�mero da sequ�ncia dos n�meros primos. Veja o exemplo abaixo:

**Exemplo:**

O s�timo (7�) n�mero da sequ�ncia Saw � 41.

**Saw:**      0	2	5	10	17	28	41	58

**Primos:**   2	3	5	7	11	13	17	...


### Resposta ###
**9854**

**__________________**

## Materiais (ambienta��o): ##

* 02 correntes ou 02 tornozeleiras de ferro
* 02 cadeados com senha
* 02 objetos que representem uma bomba (seria interessante algo com um contador)

**__________________**

## Playlist (ambienta��o): ##

* https://www.youtube.com/watch?v=oU-CQ4xeRBs
